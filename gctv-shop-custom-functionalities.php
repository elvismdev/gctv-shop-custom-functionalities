<?php
/**
 * Plugin Name: GCTV Shop Custom Functionalities
 * Plugin URI: https://bitbucket.org/grantcardone/gctv-shop-custom-functionalities
 * Description: Plugin to put customizations and override default actions of core WordPress and plugins or place some new functionalities as well.
 * Version: 1.0.0
 * Author: Elvis Morales
 * Author URI: http://elvismdev.io/
 * Requires at least: 3.8.1
 * Tested up to: 4.4.2
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Override WooCommerce Social Checkout heading if the order doesn't contains any tangible/shippable products.  
function gctvscf_override_wcsc_heading( $settings ) {	
	global $wp;
	$_wcpf = new WC_Product_Factory();

	$order_id = absint( $wp->query_vars['order-received'] );
	$order = new WC_Order( $order_id );
	$items = $order->get_items();

	foreach ( $items as $item ) {
		$_product = $_wcpf->get_product( $item['product_id'] );
		if ( !$_product->is_downloadable() || !$_product->is_virtual() ) return $settings;
	}

	$settings['wc_social_checkout_heading'] = 'Let people know you invested in yourself!';
	return $settings;

}

function gctvscf_woocommerce_loaded() {
	add_filter( 'woocommerce_sc_settings', 'gctvscf_override_wcsc_heading', 10, 1 );
}

add_action( 'woocommerce_init', 'gctvscf_woocommerce_loaded' );



?>
